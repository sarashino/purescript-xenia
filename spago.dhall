{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ name = "xenia"
, dependencies = [ "console"
                 , "effect"
                 , "prelude"
                 , "psci-support"
                 , "argonaut"
                 , "web-dom" ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
}
