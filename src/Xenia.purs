module Xenia
    ( cloneContent
    , insert
    , render
    , update
    , XeniaElem(..)
    , XeniaNodes(..)) where

import Prelude (Unit, ($), pure, (=<<), bind)
import Effect (Effect)
import Data.Maybe (Maybe(..))
import Data.Argonaut.Encode (class EncodeJson)
import Web.DOM (Element, Node)

data XeniaElem = XeniaElem Element
data XeniaNodes = XeniaNodes (Array Node)

foreign import cloneContent :: Element -> Maybe Element

foreign import insert_ :: Element -> Element -> Effect (Array Node)
insert :: XeniaElem -> Element -> Effect XeniaNodes
insert (XeniaElem el) target = do
                            ns <- insert_ el target
                            pure $ XeniaNodes $ ns

foreign import removeNodes_ :: Array Node -> Effect Unit
removeNodes :: XeniaNodes -> Effect Unit
removeNodes (XeniaNodes ns) = removeNodes_ ns

foreign import render_ :: forall a. EncodeJson (Record a) => Element -> (Record a) -> Maybe Element
render :: forall a. EncodeJson (Record a) => Element -> (Record a) -> Maybe XeniaElem
render el a = maybeXeniaElem =<< render_ el a
    where
        maybeXeniaElem :: Element -> Maybe XeniaElem
        maybeXeniaElem e = Just (XeniaElem e)

foreign import update_ :: forall a. EncodeJson (Record a) => Array Node -> (Record a) -> Effect (Array Node)
update :: forall a. EncodeJson (Record a) => XeniaNodes -> (Record a) -> Effect XeniaNodes
update (XeniaNodes ns) a = do
                        rns <- update_ ns a
                        pure $ XeniaNodes $ rns
