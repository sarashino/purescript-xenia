"use strict";

exports.cloneContent = function(el){
    let content = el.content;
    if (content == null) {
        return Data_Maybe.Nothing.value;
    }else{
        return Data_Maybe.Just.create(content.cloneNode(true));
    }
};
exports.insert_ = function(el){
    return function(target){
        let l = target.childNodes.length;
        target.appendChild(el);
        return [...target.childNodes].slice(l);
    };
};
exports.removeNodes_ = function(nodes){
    nodes.map(function(n){n.remove();});
};
exports.render_ = function(dfrag){
    return function(r){
        function renderTextNode(n){
            try{
                content = n.textContent;
                n.textContent = eval(`\`${n.textContent}\``);
                if (content != n.textContent){
                    n.parentNode.insertBefore(document.createComment(content), n);
                };
            } catch(e){
                return Data_Maybe.Nothing.value;
            }
        };
        function renderChildNode(node){
            let attridraw = `${node.id}`;
            let attrid = eval(`\`${node.id}\``);
            let attrclassraw = `${node.className}`;
            let attrclass = eval(`\`${node.className}\``);
            var changed = false;
            if (attrid != attridraw){
                changed = true;
                node.id = attrid;
            }
            if (attrclass != attrclassraw){
                changed = true;
                node.className = attrclass;
            };
            if(changed){
                let attr = `id=${attridraw};class=${attrclassraw}`
                node.parentNode.insertBefore(document.createComment(attr), node);
            }
            renderer(node);
        };
        function renderer(node){
            [...node.childNodes].map(function(n){
                if(n.nodeType == 1){
                    renderChildNode(n);
                }else if(n.nodeType == 3){
                    renderTextNode(n);
                }
            });
        };
        renderer(dfrag);
        return Data_Maybe.Just.create(dfrag);
    };
};
exports.update_ = function(nodes){
    return function(r){
        function updateTextNode(n, comment){
            n.textContent = eval(`\`${comment}\``);
        };
        function updateChildNode(n, comment){
            let [a, b] = comment.split(';');
            let tid = a.split('=')[1];
            let tclass = b.split('=')[1];
            if(tid) n.id = eval(`\`${tid}\``);
            if(tclass) n.className = eval(`\`${tclass}\``);
            updater([...n.childNodes]);
        };
        function updater(nodes){
            nodes.reduce(function(comment, n){
                if(n.nodeType == 8){
                    return n.textContent;
                }
                if(n.nodeType == 1){
                    if(comment){
                        updateChildNode(n, comment);
                    }else{
                        updater([...n.childNodes]);
                    }
                }
                if(comment && n.nodeType == 3){
                    updateTextNode(n, comment);
                    return '';
                };
            }, '');
        };
        updater(nodes);
        return nodes;
    };
};
